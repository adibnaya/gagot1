<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('gagot', 'GagotController')->middleware('auth');
Route::get('gagot/changeuser/{sid}/{uid?}', 'GagotController@changeUser')->name('gagot.changeuser');

Route::get('gagot/delete/{id}', 'GagotController@destroy')->name('gagot.delete');

Route::get('gagot/changestatus/{sid}/{staid}', 'GagotController@changeStatus')->name('gagot.changestatus')->middleware('auth');

Route::resource('users', 'UserController')->middleware('auth');
Route::get('users/changeuserrole/{userid}/{roleid}', 'UserController@changeUserRole')->name('users.change-user-role')->middleware('auth');
Route::get('users/delete/{id}', 'UserController@destroy')->name('users.delete');

Route::resource('customers', 'CustomerController')->middleware('auth');
Route::get('customers/delete/{id}', 'CustomerController@destroy')->name('customers.delete');

Route::get('statistics', 'StatisticsController@index')->name('statistics.index');

Auth::routes(['register' => false]);
