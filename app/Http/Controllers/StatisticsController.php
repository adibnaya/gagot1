<?php

namespace App\Http\Controllers;

Use App\Sale;
Use App\User;
Use App\Status;



class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('statistics.index');

    }
}
