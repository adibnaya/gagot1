<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Wood;
use Illuminate\Http\Request;
Use App\Sale;
Use App\User;
Use App\Status;



class GagotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->role->name === 'admin'){
            $saleQuery = Sale::query();
        } else {
            $saleQuery = Sale::where('user_id', auth()->id());
        }

        if(isset(request()->user)) {
            $saleQuery->where('user_id', request()->user);
        }
        if(isset(request()->customer)) {
            $saleQuery->where('customer_id', request()->customer);
        }
        if(isset(request()->wood)) {
            $saleQuery->where('wood_id', request()->wood);
        }
        if(isset(request()->status)) {
            $saleQuery->where('status_id', request()->status);
        }

        $sales = $saleQuery->sortable()->paginate(10);
        $users = User::all();
        $customers = Customer::all();
        $statuses = Status::all();
        $wood = Wood::all();
        return view('gagot.index', compact('sales','users', 'statuses', 'customers', 'wood'));

    }

    public function changeUser($sid,$uid=null){
        $sale=Sale::findOrFail($sid);
        $sale->user_id=$uid;
        $sale->save();
        return redirect('gagot');
    }   

    public function changeStatus($sid, $staid)
    {
        $sale = Sale::findOrFail($sid);
        $sale->status_id = $staid;
        $sale->save();
        return redirect('gagot');
    }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        $users = User::all();
        $wood = Wood::all();
        return view('gagot.create', compact('customers', 'users', 'wood'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sale = new Sale($request->all());
        $sale->status_id = Status::where('name', 'pending payment')->first()->id;
        $sale->save();
        return redirect ('gagot');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sale::findOrFail($id);
        $customers = Customer::all();
        $users = User::all();
        $wood = Wood::all();
        return view ('gagot.edit', compact('sale', 'customers', 'users', 'wood'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sale = Sale::findOrFail($id);
        $sale->update($request->all());
        return redirect ('gagot');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sale::findOrFail($id);
        $sale->delete();
        return redirect('gagot');
    }
}
