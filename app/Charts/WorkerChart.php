<?php

declare(strict_types=1);

namespace App\Charts;

use App\Sale;
use App\User;
use App\Wood;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class WorkerChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $worker = Sale::all()->groupBy('user_id')->map(function ($sale) {
            return $sale->sum('amount');
        });
        $keys = $worker->keys()->map(function ($key){
            return User::find($key)->name;
        });
        return Chartisan::build()
            ->labels($keys->toArray())
            ->dataset('Sales By Worker', $worker->values()->toArray());
    }
}