<?php

declare(strict_types=1);

namespace App\Charts;

use App\Sale;
use Carbon\Carbon;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class SalesChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $sales = Sale::all()->sortBy(function ($val) {
            return Carbon::parse($val->supply_date)->format('m');
        })->groupBy(function ($val) {
            return Carbon::parse($val->supply_date)->format('M');
        })->map(function ($months) {
            return $months->count();
        });
        return Chartisan::build()
            ->labels($sales->keys()->toArray())
            ->dataset('Sales By Month', $sales->values()->toArray());
    }
}