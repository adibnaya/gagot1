<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Wood extends Model
{
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }
}

