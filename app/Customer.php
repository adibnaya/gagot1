<?php


namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Customer extends Model
{
    use Sortable;

    public $timestamps = false;

    protected $fillable = ['name'];

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }
}

