<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Kyslik\ColumnSortable\Sortable;

class Sale extends Model
{
    use Sortable;

    public $sortable = ['user_id',
        'customer_id',
        'status_id',
        'wood_id',
        'amount',
        'supply_date'];

    protected $fillable = ['customer_id', 'user_id', 'wood_id', 'amount', 'supply_date'];
    protected $dates = ['supply_date'];

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function wood()
    {
        return $this->belongsTo('App\Wood');
    }

    public function getSupplyDateAttribute($date)
    {
        return Carbon::make($date)->toDateString();
    }

}
