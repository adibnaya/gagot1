@extends('layouts.app')

@section('title','Statistics')

@section('content')
    <div>
        <h1>Statistics</h1>
    </div>
    <div id="chart" style="height: 300px; width: 100%;"></div>
    <div id="chart2" style="height: 800px; width: 100%;"></div>
    <div id="chart3" style="height: 800px; width: 100%;"></div>
    <script>
      const chart = new Chartisan({
        el: '#chart',
        url: "@chart('sales_chart')",
        hooks: new ChartisanHooks().legend().title('Sales By Month')
        .colors()
        .datasets(['line']),
      });
      const chart2 = new Chartisan({
        el: '#chart2',
        url: "@chart('wood_chart')",
        hooks: new ChartisanHooks().title('Wood Sales By Type')
        .colors()
        .datasets(['pie'])
        .tooltip(),
      });
      const chart3 = new Chartisan({
        el: '#chart3',
        url: "@chart('worker_chart')",
        hooks: new ChartisanHooks().title('Sales By Worker')
        .colors()
        .datasets(['pie'])
        .tooltip(),
      });
    </script>
@endsection
                        