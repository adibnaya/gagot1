@extends('layouts.app')

@section('title','Create new Customer')

@section('content')

    <h1>Create Customer</h1>
    <form method="post" action="{{action('CustomerController@store')}}">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div>
            <input type="submit" name="submit" value="Create Customer">
        </div>
    </form>
@endsection
