@extends('layouts.app')

@section('title','Edit Customer')


@section('content')

    <h1>Edit Customer</h1>
    <form method="post" action="{{action('CustomerController@update', $customer->id)}}">
        @method('PATCH')
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value= {{$customer->name}}>
        </div>
        <div>
            <input type="submit" name="submit" value="Update Customer">
        </div>
    </form>
@endsection