@extends('layouts.app')

@section('title','Edit User')


@section('content')

    <h1>Edit User</h1>
    <form method="post" action="{{action('UserController@update', $user->id)}}">
        @method('PATCH')
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value= {{$user->name}}>
        </div>
        <div class="form-group">
            <label for="name">Email</label>
            <input type="text" class="form-control" name="email" value= {{$user->email}}>
        </div>
        <div class="form-group">
            <label for="password">Update Password</label>
            <input type="password" class="form-control" name="password">
        </div>
        <div>
            <input type="submit" name="submit" value="Update User">
        </div>
    </form>
@endsection