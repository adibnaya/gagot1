@extends('layouts.app')

@section('title','Users')

@section('content')
    <div>
        <a href="{{url('/users/create')}}" class="btn btn-info">Add new user</a>
        <h1>List of Users</h1>
    </div>
    <table class="table">
        <tr>
            <th>@sortablelink('name')</th>
            <th>@sortablelink('email')</th>
            <th>@sortablelink('role_id', 'Role')</th>
            <th></th>
        </tr>

        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle"
                                type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                                {{$user->role->name}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($roles as $role)
                                <a class="dropdown-item"
                                   href="{{route('users.change-user-role',[$user->id,$role->id])}}">{{$role->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </td>
                <td>
                    <a href="{{route('users.edit',$user->id)}}">Edit</a>
                </td>
                <td>
                    <a href="{{route('users.delete',$user->id)}}">Delete</a>
                </td>
            </tr>

        @endforeach
    </table>
    {{$users->links()}}
@endsection
                        