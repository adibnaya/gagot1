<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use App\Sale;
use App\Status;
use App\User;
use App\Wood;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Sale::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomElement(User::all()),
        'customer_id' => $faker->randomElement(Customer::all()),
        'status_id' => $faker->randomElement(Status::all()),
        'wood_id' => $faker->randomElement(Wood::all()),
        'amount' => $faker->numberBetween(10, 1000) * 10,
        'supply_date' => $faker->dateTimeBetween('-2 years', '+1 year'),
    ];
});
