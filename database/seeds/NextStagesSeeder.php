<?php

use App\Status;
use Illuminate\Database\Seeder;

class NextStagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nextstages')->insert([
            [
                'from' => Status::where('name', 'pending payment')->first()->id,
                'to' => Status::where('name', 'paid')->first()->id,
            ],
            [
                'from' => Status::where('name', 'pending payment')->first()->id,
                'to' => Status::where('name', 'delivered')->first()->id,
            ],
            [
                'from' => Status::where('name', 'paid')->first()->id,
                'to' => Status::where('name', 'delivered')->first()->id,
            ],

        ]);

    }
}
