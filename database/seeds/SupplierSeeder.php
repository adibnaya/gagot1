<?php

use App\Category;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->insert([
            [
                'name' => 'Tubul&Sons',
            ],
            [
                'name' => 'Hadarim Wood Industries',
            ],
            [
                'name' => 'Rame Trees',
            ],
            [
                'name' => 'Erez Wood Storage and Tiles',
            ],
        ]);
    }
}