<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CategorySeeder::class);
         $this->call(RoleSeeder::class);
         $this->call(StatusSeeder::class);
         $this->call(CustomerSeeder::class);
         $this->call(SupplierSeeder::class);
         $this->call(WoodSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(SaleSeeder::class);
         $this->call(NextStagesSeeder::class);
    }
}
